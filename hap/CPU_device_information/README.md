# CPU信息应用(查看cpu利用率)

开发环境
- API10
- 硬件：OpenHarmony开发者手机
- OpenHarmony 4.O.10.309

## 应用安装的步骤

```
# 替换掉/vendor/etc下的init.uis7885.cfg文件，给proc/stat 777权限
hdc shell mount -o remount,rw /vendor
hdc file send init.uis7885.cfg /vendor/etc
hdc shell reboot 

hdc install entry-default-signed.hap
```

这里已经写好了脚本，点击[init.bat](./init.bat)直接安装。

## 应用实现的功能

应用通过Native C++方式开发,实现的功能:获取设备如下信息
- CPU核心数
- SOC型号
- GPU温度
- 主板温度
- 系统运行时间
- RAM总内存
- RAM可用内存
- RAM空闲内存
- 缓存使用内存
- Swaps交换分区
- 系统启动以来创建的进程数
- 上下文切换的总数
- SOC温度
- CPU利用率
- CPU大核7温度和利用率
- CPU中核6温度和利用率
- CPU中核5温度和利用率
- CPU中核4温度和利用率
- CPU小核3温度和利用率
- CPU小核2温度和利用率
- CPU小核1温度和利用率
- CPU小核0温度和利用率
- 设备电量
- 电池电压
- 电池型号
- 电池充电状态
- 系统版本
- RTC时间和日期
- 内核版本信息
- 电池信息
 
![](./media/snapshot_1970-01-01_09-10-54.jpeg)
