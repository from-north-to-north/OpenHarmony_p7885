# 全局水印demo

应用第三方作者，https://gitee.com/scenario-samples/water-mark-view/tree/master

# 介绍
* 使用组件默认属性overlay实现组件级水印效果
  
# 约束与限制
1. 运行环境
    * 手机ROM版本：ALN-AL00 5.0.0.102(SP8C00E73R4P17logpatch02)
    * IDE：DevEco Studio 5.0.3.910
    * SDK：HarmonyOS 5.0.0 Release（API 12）

# 工程目录
```
entry/src/main/ets/
|---entryability
|   |---EntryAbility.ets
|---pages
|   |---Index.ets                   // 入口页面
|   |---WaterMarkView.ets           // 水印页面
```

# 效果预览
![image.png](./image.png)