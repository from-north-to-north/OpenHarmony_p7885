应用来源
- https://gitee.com/scenario-samples/long-task
- https://ost.51cto.com/posts/31807

# LongTask
## 介绍
本工程通过Background Tasks Kit实现应用退出后台后继续播放网络音频。
## 约束和限制
API12及以上。
需要权限：
1、ohos.permission.INTERNET
2、ohos.permission.KEEP_BACKGROUND_RUNNING
## 效果预览
![输入图片说明](Pictures/%E5%90%8E%E5%8F%B0%E6%92%AD%E6%94%BE%E5%9C%A8%E7%BA%BF%E9%9F%B3%E9%A2%91.gif)
## 工程目录

```c
entry\src\main\ets
├─entryability
│      EntryAbility.ets
│
├─entrybackupability
│      EntryBackupAbility.ets
│
└─pages
        Index.ets               // 首页
```
