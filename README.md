# OpenHarmony开发者手机开发样例

- 笔者个人微信：OpenHarmony_lbkg，有问题欢迎咨询

## 开发者手机应用

- [手机本地访问shell终端](./hap/shell_hap)

- [开发者手机运行大语言模型](./hap开发者手机运行大语言模型)

- [cpu信息应用](./hap/CPU_device_information)

- [cpu信息应用(悬浮屏版本)](./hapCpu_info_float_windows)

- [开发者手机浏览器](./hap手机浏览器)



## 应用开发简单样例

### API12

- [应用跳转](./hap/easy_demo/demo_tiaozhuan)

- [聊天框样例](./hap/easy_demo/xx_chat)
  
- [全局水印demo](./hap/easy_demo/water-mark-view-master)

- [播放在线音频](./hap/easy_demo/long-task-master)

- [多段音频自动连续播放](./hap/easy_demo/Continuous_play)

### API10

- [读取rawfile文件内容](./hap/easy_demo/rawfile/README.md)
- [程序访问控制管理样例](./hap/easy_demo/abilityAccessCtrl/README.md)
  - 比如添加使用麦克风的权限
- [悬浮屏幕应用](./hap/easy_demo/float_windows/README.md)  
- 添加悬浮屏，使得应用可以悬浮在其他应用上 
- [应用信息查询](./hap/easy_demo/bundleName_demo/README.md)
     - 查询应用信息 
- [tcp调试程序](./hap/easy_demo/tcp_demo)  
- [设置应用窗口状态栏和导航栏属性](./hap/easy_demo/window_demo/README.md)
     - 设置导航栏和状态栏的颜色和字体颜色
- [组件快捷键事件](./hap/easy_demo/keyboardShortcut/README.md)
     - 将开发者手机连接键盘，使用键盘上的F1或者Ctrl等快捷键拉起特定事件 
- [音效设置](./hap/easy_demo/voice)

- HarmonyOS NEXT样例
   - https://gitee.com/harmonyos-cases/cases
   - https://gitee.com/scenario-samples/demo-index


## 开发者手机烧录工具

- [烧录工具](./烧录工具)
   - 注意：第二代开发者手机：长按电源+音量减 ，再插入电源才能烧写

## 开发者手机使用文档

- [开发者手机 - 分发中心 - 应用上架指南](https://laval.csdn.net/6644606956fe1f7e9efa2dee.html)

- [开发者手机版本计划 & 开发进展](https://laval.csdn.net/652165d4aae4e179c0b780aa.html?login=from_csdn)

- [开发者手机源码编译](./hap/laval_phone_docx/开发者手机源码编译/开发者手机源码编译.md)
   - [开发者手机烧录固件](https://laval.csdn.net/65c33699dafaf23eeaee8610.html?login=from_csdn&login=from_csdn)

- [开发者手机如何强制关机](./hap/laval_phone_docx/开发者手机如何强制关机.md)
  
- [开发者手机强制重启](./hap/laval_phone_docx/开发者手机强制重启.md)

- [锁屏壁纸替换](./hap/laval_phone_docx/锁屏壁纸替换/锁屏壁纸替换.md)

- [开发者手机launcher应用编译_桌面壁纸替换](./hap/laval_phone_docx/开发者手机launcher应用编译_桌面壁纸替换/2.8_2.8_开发者手机launcher应用编译_桌面壁纸替换.md)

- [开发者手机-292SG主板堆叠说明书](./hap/laval_phone_docx/开发者手机-292SG主板堆叠说明书/3292SG主板堆叠说明书.pptx)

- [开发者手机如何增加或者减少分区](https://gitee.com/develop-phone-open-source/device_board_hys/pulls/5/files)

- [使用hdc TCP模式无线方式连接OpenHarmony设备](./hap/laval_phone_docx/使用hdc_TCP模式无线方式连接OpenHarmony设备.md)

## OpenHarmony压测应用

- [elec_wukong](https://gitee.com/wshikh/elec_wukong/tree/master)
  - [二进制下载](https://gitee.com/wshikh/elec_wukong/blob/master/src/out/eewukong-win-3.2.0-x64.exe) 

## ArkUI应用开发文档

- [开发者论坛 HarmonyOS NEXT讨论区](https://developer.huawei.com/consumer/cn/forum/tag/harmonyosnext?pageIndex=1&filterCondition=1&fid=0109140870620153026)

- [如何实现一个组件不停地旋转](https://developer.huawei.com/consumer/cn/forum/topic/0204141129854777235?fid=0109140870620153026)

- [react-native-for-harmony](https://gitee.com/jiandong001/react-native-for-harmony#211-arkjs%E5%92%8Ces%E7%9A%84gap)

- [如何在图片显示的分辨率](https://developer.huawei.com/consumer/cn/forum/topic/0207146175075426264?fid=0109140870620153026)

- 查看开发者手机屏幕信息 `hdc shell hidumper -s 10 -a screen`

- 查看当前窗口信息 `hdc shell hidumper -s WindowManagerService -a '-a'`

- 获取应用信息`hdc shell aa dump -a`

- [hilog和console.log的区别](./ArkUI_docx/hilog和console.log的区别/hilog和console.log的区别.md)

## 开发者手机介绍



- 第一代
![](./hap/media/weChatShopBanner0.png)

- 第二代：增加指纹识别，重力加速度传感器
![Alt text](./hap/media/image.png)
开发者手机第二代通过OpenHarmony 4.1 release通过兼容性测评
![Alt text](./hap/media/dqwedwq.png)