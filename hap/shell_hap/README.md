# 本地访问shell终端应用


## 安装步骤

- 下载仓库至本地，点击`init.bat`脚本安装本应用

## 实现本地访问shell终端思路

1.首先移植[ttyd开源项目](https://github.com/tsl0922/ttyd)到OpenHarmony标准系统。

- [ttyd_oh_install.tar](ttyd_oh_install.tar)是移植好的ttyd二进制文件

2.通过修改init.uis7885.cfg开机自动启动ttyd

3.开发web应用实现本地访问shell


### ttyd_api10_1.0
|                                          |                                          |                                          |                                          | 
| ---------------------------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
|![](./media/ttyd_api10_1.0/snapshot_2024-02-15_13-53-11.jpeg) | ![](./media/ttyd_api10_1.0/snapshot_2024-02-15_13-53-31.jpeg) | ![](./media/ttyd_api10_1.0/snapshot_2024-02-15_13-54-09.jpeg) | ![](./media/ttyd_api10_1.0/snapshot_2024-02-15_13-55-06.jpeg) |


### ttyd_api10_3.0

- [ttyd_api10_3.0](./hap源码/ttyd_api10_3.0/) : 根据[提升应用冷启动速度](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/performance/improve-application-cold-start-speed.md)文档提高应用启动速度

|                                          |                                          |   
| ---------------------------------------- | ---------------------------------------- | 
|![](./media/ttyd_api10_3.0/snapshot_2024-03-30_20-01-10.jpeg) |![](./media/ttyd_api10_3.0/snapshot_2024-03-30_20-01-28.jpeg)  |
