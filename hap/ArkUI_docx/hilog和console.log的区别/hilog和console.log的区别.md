![](./image.png)

从上图可以看到，日志在打印的时候，会包含4部分内容：domaid、tag、日志级别、日志内容。

hilog可以设置domaid、tag、日志级别，且hilog的domaid不需要自定义，系统当前已经定义好了。

console.log的domaid、tag、日志级别是固定的，console.log打印时，domaid为‘A0c0d0’，tag为‘JSApp’，日志级别为info。所以使用console.log打印日志，不方便日志定位