# 将 CPU 调频模式切换为性能模式，这意味着用户程序可以手动控制 CPU 的工作频率，而不是由系统自动管理。这样可以提供更大的灵活性和定制性，但需要注意合理调整频率以保持系统稳定性和性能。
echo performance > /sys/devices/system/cpu/cpufreq/policy0/scaling_governor

# 输出提示信息
echo "View CPU frequency."
# 查看 CPU 频率
cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_cur_freq
echo "------------------------------------------"

# 输出提示信息
echo "Initialize language model."
chmod 777 llama  
# 启动语言模型
./llama -m chinese-alpaca-7b-q4.bin -t 8  
