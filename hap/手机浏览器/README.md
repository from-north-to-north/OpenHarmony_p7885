## 主要功能

- 本应用已合并至 [LAVAL应用共建团队/Laval-Browser](https://gitee.com/laval-applications/laval-browser) 仓库

本应用基于[Laval-Browser](https://gitee.com/laval-applications/laval-browser?_from=gitee_search)改造，新增全屏模式、横屏模式、深色模式、电脑模式。

- [laval-browser_v100](./hap源码/laval-browser_v100/) ：全屏模式、横屏模式、深色模式、电脑模式
- [laval-browser_v101](./hap源码/laval-browser_v101/) ：新增网页开启关闭静音
- [laval-browser_v102](./hap源码/laval-browser_v102/) ：优化深色模式
- [laval-browser_v103](./hap源码/laval-browser_v103/) : 优化深色模式
- [laval-browser_v104](./hap源码/laval-browser_v104/) : 根据[Web组件开发性能提升指导](https://gitee.com/harmonyos-cases/cases/blob/master/docs/performance/performance-web-import.md) 提升浏览器性能
- [laval-browser_v105](./hap源码/laval-browser_v105/) : 增加网页视频全屏时自动全屏的功能
- [laval-browser_v106](./hap源码/laval-browser_v106/) : 完善网页视频全屏时自动全屏的功能
- [laval-browser_v107](./hap源码/laval-browser_v107/) : 根据[提升应用冷启动速度](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/performance/improve-application-cold-start-speed.md)文档提高应用启动速度

- [laval-browser_v200](./hap源码/laval-browser_v200/) : 重构浏览器应用，新增多标签页功能

### v1.0.0

|                                          |                                          |                                          |                                          | 
| ---------------------------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| ![](./media/v1.0.0/snapshot_2024-02-18_17-50-46.jpeg)| ![](./media/v1.0.0/snapshot_2024-02-18_17-50-41.jpeg) |  ![](./media/v1.0.0/snapshot_2024-02-18_17-51-04.jpeg) | ![](./media/v1.0.0/snapshot_2024-02-18_17-57-09.jpeg) |


### v1.0.7
|                                          |                                          |                                          |                                          | 
| ---------------------------------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| ![](./media/v1.0.7/snapshot_2024-03-30_18-35-09.jpeg)| ![](./media/v1.0.7/snapshot_2024-03-30_18-35-21.jpeg) |  ![](./media/v1.0.7/snapshot_2024-03-30_18-35-42.jpeg) | ![](./media/v1.0.7/snapshot_2024-03-30_18-39-44.jpeg) |