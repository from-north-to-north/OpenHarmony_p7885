
import window from '@ohos.window';
import common from '@ohos.app.ability.common';
import display from '@ohos.display';

const TAG = 'FloatWindowController';
const FLOAT_SIZE = vp2px(120);

/**
 * 悬浮窗控制类
 */
export default class FloatWindowController {

  // 创建单例模式
  private static mInstance: FloatWindowController | null = null;

  public static getInstance(): FloatWindowController {
    if (FloatWindowController.mInstance == null) {
      FloatWindowController.mInstance = new FloatWindowController();
    }
    return FloatWindowController.mInstance;
  }

  private constructor() {
    // 私有化构造函数
  }

  private floatWindow: window.Window | null = null;
  private windowStage: window.WindowStage | null = null;

  //ability调用，监听windowStage状态变化
  async initWindowStage(context: common.UIAbilityContext, windowStage: window.WindowStage) {
    this.windowStage = windowStage;
    let mainWin = await windowStage.getMainWindow();

    await mainWin.setWindowSystemBarProperties({
      statusBarColor: '#182431',
      navigationBarColor: '#182431'
    });

    windowStage.on('windowStageEvent', (event: window.WindowStageEventType) => {
      if (event === window.WindowStageEventType.SHOWN) {
        this.destroyFloatWindow();
      } else if (event === window.WindowStageEventType.HIDDEN) {
        this.createAndShowFloatWindow(context);
      }
    });
  }

  //此处有一个bug，当前ability调用windowStage.getMainWindow().hide()方法，会与上一个ability一起hide
  async hideMain() {
    if(this.windowStage){
      let mainWin: window.Window = await this.windowStage.getMainWindow();
      mainWin.hide();
    }
  }


  //创建悬浮窗
  private async createAndShowFloatWindow(context: common.UIAbilityContext) {
    if (context.isTerminating()) {
      return;
    }

    console.info(TAG,` createAndShowWindow`);

    let w = display.getDefaultDisplaySync().width;//获取屏幕宽度
    let h = display.getDefaultDisplaySync().height;//获取屏幕宽度

    // 创建应用子窗口
    this.floatWindow = await window.createWindow({
      name: 'DemoFloatWindow',
      windowType: window.WindowType.TYPE_FLOAT,
      ctx: context
    });

    // 设置悬浮窗位置
    await this.floatWindow.moveWindowTo(w - FLOAT_SIZE - 200, h - FLOAT_SIZE - 800);

    // 设置悬浮窗大小
    await this.floatWindow.resize(FLOAT_SIZE + 185, FLOAT_SIZE + 130);

    //// 为悬浮窗加载页面内容，这里可以设置在main_pages.json中配置的页面

    await this.floatWindow.setUIContent('pages/Float');

    this.floatWindow.setWindowBackgroundColor('#0009cf40');

    // 显示悬浮窗。
    await this.floatWindow.showWindow();

  }

  // 自定义销毁悬浮窗方法
  public async destroyFloatWindow() {
    console.info(TAG,` destroyWindow`);
    if (this.floatWindow) {
      await this.floatWindow.destroyWindow();
    }
  }

}
