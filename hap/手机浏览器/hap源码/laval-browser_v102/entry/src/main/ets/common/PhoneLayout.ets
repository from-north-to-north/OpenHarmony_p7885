/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Browser } from '../model/Browser'
import { WebTab } from './TitleBar'
import Logger from '../model/Logger'

import window from '@ohos.window';
import common from '@ohos.app.ability.common';


const TAG: string = '[PhoneLayout]'
const BUTTON_WIDTH: number = 25
const BUTTON_RADIUS: number = 4
const DOWN_COLOR: string = '#e4e4e4'
const UP_COLOR: string = '#00000000'

@Component
export struct PhoneLayout {
  @Link browser: Browser;
  @Link isPhone: boolean ;
  @State hasDown: boolean = false;
  @State pageCount: string = '1';
  @State arrayIndex: number = 0;

  //深色模式
  @State access: boolean = false;
  @State mode: WebDarkMode = WebDarkMode.Off;

  private addr: string = ''
  private toolPoneArray: Array<{
    imageSrc: Resource,
    id: number
  }> = [
    {
      imageSrc: $r('app.media.ic_public_back'),
      id: 1
    },
    {
      imageSrc: $r('app.media.ic_public_advance'),
      id: 2
    },
    {
      imageSrc: $r('app.media.ic_public_home'),
      id: 5
    },
    {
      imageSrc: $r('app.media.ic_public_refresh'),
      id: 3
    },
    {
      imageSrc: $r('app.media.ic_gallery_full_screen'),
      id: 6
    },
    {
    imageSrc: $r('app.media.ic_controlcenter_dark_filled'),
    id: 7
    },
    {
      imageSrc: $r('app.media.ic_public_multiscreen'),
      id: 8
    },
    {
      imageSrc: $r('app.media.ic_device_matebook_filled'),
      id: 9
    }
  ]

  //沉浸式界面开发:https://gitee.com/openharmony/docs/blob/master/zh-cn/third-party-cases/immersion-mode.md#%E5%8F%82%E8%80%83
  context: common.UIAbilityContext = getContext(this) as common.UIAbilityContext
  @State isFirstCall: boolean = false;
  async setSystemBar(isFirstCall: boolean) {
    if (isFirstCall) {
      let names: Array<'status' | 'navigation'> = [];
      let windowClass = await window.getLastWindow(this.context)
      await windowClass.setWindowSystemBarEnable(names);
    } else {
      let names: Array<'status' | 'navigation'> = ['navigation','status'];
      let windowClass = await window.getLastWindow(this.context)
      await windowClass.setWindowSystemBarEnable(names);
    }
  }

  // 横竖屏切换
  @State isFullScreen:boolean = false
  horVerSwitch(){
    let context = getContext(this) as common.UIAbilityContext;
    // 使用getLastWindow获取当前窗口
    window.getLastWindow(context).then((lastWindow)=>{
      // 使用setPreferredOrientation实现横竖屏切换
      lastWindow.setPreferredOrientation(this.isFullScreen ? window.Orientation.PORTRAIT : window.Orientation.LANDSCAPE)
      this.isFullScreen = !this.isFullScreen
    })
  }

  @State clickCount: number = 0;
  @State clickCount1: number = 0;
  @State aaa: string = "5%";
  @State color: string = '#ffffffff';


  @Builder ToolBar() {
    Column() {
      Row() {

        ForEach(this.toolPoneArray, item => {
          Column() {
            Divider().color('#ffffffff')
            Button({ type: ButtonType.Normal }) {
              Column() {
                if (item.id !== 4) {
                  Image(item.imageSrc)
                } else {
                  Column() {
                    Text(this.pageCount)
                      .fontSize(16)
                  }
                  .border({ width: 2 })
                  .width(22)
                  .height(22)
                  .borderRadius(5)
                  .justifyContent(FlexAlign.Center)
                }
              }
              .width(BUTTON_WIDTH)
              .height(BUTTON_WIDTH)
              .justifyContent(FlexAlign.Center)
            }
            .height('100%')
            .width('100%')
            .backgroundColor(this.color)
            .borderRadius(BUTTON_RADIUS)
            .flexShrink(0)
            .onTouch((event: TouchEvent) => {
              if (event.type === TouchType.Down) {
                this.arrayIndex = item.id
              } else if (event.type === TouchType.Up) {
                this.arrayIndex = 0
              }
            })
            .onClick((event: ClickEvent) => {
              switch (item.id) {
                case 1:
                  this.browser.Back()
                  break;
                case 2:
                  this.browser.Forward()
                  break;
                case 3:
                  this.browser.Refresh()
                  break;
                case 5:
                  this.browser.webControllerArray[this.browser.tabArrayIndex].controller.loadUrl({
                    url: $rawfile('phone.html')
                  })
                  break;
                case 6:
                  // 根据点击次数确定参数值
                  if (this.clickCount % 2 === 0) {
                    this.setSystemBar(true); // 偶数次点击设置为true
                    this.aaa = "0%";
                  } else {
                    this.setSystemBar(false); // 奇数次点击设置为false
                    this.aaa = "5%";
                  }
                  this.clickCount++;
                  break;
                case 7:
                // 根据点击次数确定参数值
                  if (this.clickCount % 2 === 0) {
                    this.access=false // 偶数次点击设置为true
                    this.mode=WebDarkMode.Off;
                    this.color="#ffffffff"
                  } else {
                    this.access=true; // 奇数次点击设置为false
                    this.mode=WebDarkMode.On;
                    this.color= "#ff484444"
                  }
                  this.clickCount++;
                  break;
                case 8:
                  this.horVerSwitch()
                  break;
                case 9:
                  // 根据点击次数确定参数值
                  if (this.clickCount1 % 2 === 0) {
                    this.isPhone=false;
                    this.browser.Refresh()// 偶数次点击设置为true
                  } else {
                    this.isPhone=true;// 奇数次点击设置为false
                    this.browser.Refresh()
                  }
                  this.clickCount1++;
                  break;
                default:
                  break;

              }
            })
          }
          .backgroundColor(this.color)
          .width('12.5%')
        })
      }
      .justifyContent(FlexAlign.SpaceAround)
      .width('100%')
      .height('100%')
      .backgroundColor('#fdfdfd')

    }
    .height('100%')
  }

  build() {
    Column() {
      Navigation() {
        Column() {

          Row() {
            TextInput({ placeholder: '输入网址...', text: this.browser.inputValue })
              .placeholderFont({ size: 13, weight: 500 })
              .fontSize(16)
              .height(33)
              .width("90%")
              .onChange((value: string) => {
                Logger.info(TAG, `onChange`)
                this.addr = value
              })
              .onSubmit((enterKey: EnterKeyType) => {
                Logger.info(TAG, `onSubmit`)
                this.browser.webControllerArray[this.browser.tabArrayIndex].controller.loadUrl({
                  url: `https://${this.addr}`
                })
                this.addr = ''
              })
              .margin({top:3,bottom:3})

            Button({ type: ButtonType.Normal }) {
              Image($r('app.media.submit'))
            }
            .margin({top:3,bottom:3})
            .width(BUTTON_WIDTH)
            .height(20)
            .backgroundColor(this.hasDown ? DOWN_COLOR : UP_COLOR)
            .borderRadius(BUTTON_RADIUS)
            .flexShrink(0)
            .onTouch((event: TouchEvent) => {
              if (event.type === TouchType.Down) {
                this.hasDown = true
              } else if (event.type === TouchType.Up) {
                this.hasDown = false
              }
            })
            .onClick((event: ClickEvent) => {
              this.browser.loadUrl(this.addr)
              this.addr = ''
            })
          }
          .backgroundColor(this.color)
          .height(this.aaa)
          .width('100%')

          //网页加载进度条
          if (!this.browser.hideProgress) {
            Progress({ value: this.browser.progress, total: 100 })
              .color('#0000ff')
              .width('100%')
          }

          Column() {
            WebTab({ browser: $browser, isPhone: $isPhone , access: $access, mode:$mode})
          }





        }

      }
      .hideTitleBar(true)
      .toolBar(this.ToolBar)
      .hideBackButton(true)
      //https://docs.openharmony.cn/pages/v3.2/zh-cn/application-dev/ui/arkts-navigation-navigation.md/
      .mode(NavigationMode.Stack)//将mode属性设置为NavigationMode.Stack，Navigation组件即可设置为单页面显示模式。
    }
  }
}