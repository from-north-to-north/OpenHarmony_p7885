本文将介绍如何使用hdc工具 tcp模式以无线的方式连接OpenHarmony设备。

## 1. usb连接方式切换为tcp模式。
将usb线将OpenHarmony设备和电脑端连接，并且将两个连接至同一个局域网。
```
# 执行 tmode port port-number，port-number设置为端口号。
hdc tmode port 6666

# 执行 hdc tconn OpenHarmony设备IP地址:port-number端口号，然后把两者的USB线拔下
hdc tconn 192.168.229.195:6666

hdc shell
```
![image.png](https://dl-harmonyos.51cto.com/images/202405/c49c178382d3381247c204337fce89411c4a6f.png?x-oss-process=image/resize,w_820,h_192)

- OpenHarmony设备IP地址可以通过`wifiManager.getIpInfo().ipAddress`获取,具体见该样例[tcp_demo](https://gitee.com/from-north-to-north/OpenHarmony_p7885/blob/master/easy_demo/tcp_demo/entry-default-signed.hap)。

## 2. tcp模式切换回usb模式

```
# 将设备和电脑两者之间USB线连接
hdc tmode usb

hdc shell 
```

![image.png](https://dl-harmonyos.51cto.com/images/202405/4551bb975d065f0704c7854cef3586b4e76e70.png?x-oss-process=image/resize,w_709,h_121)

## 3. 同时启用hdc tcp和usb模式
```
# 进入shell终端
hdc shell

# 设置属性 persist.hdc.mode 为all ，同时启用hdc tcp和usb模式
param set persist.hdc.mode all

# 设置属性 persist.hdc.port 端口号
param set persist.hdc.port 6666

# 设置属性 persist.hdc.mode 为tcp ，启用hdc tcp模式
param set persist.hdc.mode tcp

# 设置属性 persist.hdc.mode 为usb ，启用hdc usb模式
param set persist.hdc.mode usb
```
