## 安装编译


```
编译安装环境
- DevEco Studio 4.0 Release
 - 构建版本：4.0.0.600, built on October 17, 2023
- API 10
```


1.本示例需要在module.json5中配置如下权限:

- 允许应用使用悬浮窗的能力：ohos.permission.SYSTEM_FLOAT_WINDOW

2.本示例使用了系统能力的接口，还需在SDK/10/toolchains/lib目录下的UnsgnedReleasedProfileTemplate.json中修改以下配置权限：

```
"bundle-info":{
  ...
  "apl":"system_core",
  "app-feature":"ohos_system_app"
}
```