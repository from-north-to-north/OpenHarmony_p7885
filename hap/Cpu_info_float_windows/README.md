## 应用安装编译

这里已经写好了脚本，下载后点击[init.bat](./init.bat)直接安装。

```
编译安装环境
- DevEco Studio 4.0 Release
 - 构建版本：4.0.0.600, built on October 17, 2023
- API 10
```


1.本示例需要在[hap源码](./hap/Cpu_info_3.8/)module.json5中配置如下权限:

- 允许应用使用悬浮窗的能力：ohos.permission.SYSTEM_FLOAT_WINDOW

2.本示例使用了系统能力的接口，需在SDK/10/toolchains/lib目录下的UnsgnedReleasedProfileTemplate.json中修改以下配置权限：

```
"bundle-info":{
  ...
  "apl":"system_core",
  "app-feature":"ohos_system_app"
}
```


3.修改手机/vendor/etc下的init.uis7885.cfg文件。

在boot字段给以下节点 644权限

```
"chmod 644 /proc/stat",
...
"chmod 644 /sys/devices/system/cpu/cpufreq/policy0/cpuinfo_cur_freq"
...
"chmod 644 /sys/devices/system/cpu/cpufreq/policy4/cpuinfo_cur_freq"
...
"chmod 644 /sys/devices/system/cpu/cpufreq/policy7/cpuinfo_cur_freq"
```


```
hdc shell mount -o remount,rw /vendor
hdc file send init.uis7885.cfg /vendor/etc
hdc shell reboot 
```

4.打开DevEco Studio 4.0 Release，编译安装应用。

## 应用效果展示
|                                          |                                          |    
| ---------------------------------------- | ---------------------------------------- |
|![](./media/snapshot_2024-03-11_08-40-34.jpeg)|![](./media/snapshot_2024-03-11_08-41-37.jpeg)|
|![](./media/snapshot_2024-03-11_08-42-17.jpeg)|![](./media/snapshot_2024-03-11_08-46-56.jpeg)|