1.下载模型[chinese-alpaca-7b-q4.bin](https://huggingface.co/kewin4933/InferLLM-Model/tree/main)

![Alt text](./media/image.png)

2.将下载好的chinese-alpaca-7b-q4.bin发送到开发板data目录

```
hdc file send chinese-alpaca-7b-q4.bin data
```

![Alt text](./media/image1.png)

3.将[llama](./llama)和[llama.sh](./llama.sh)同时发送至开发板data

```
hdc file send llama data
hdc file send llama.sh data
```

4.按[本地访问shell终端应用(API9)](../shell_hap(API9)/README.md#安装步骤)步骤安装进入shell终端执行

```
cd data
chmod 777 llama.sh
./llama.sh
```

![](./media/snapshot_2024-02-15_15-13-24.jpeg)