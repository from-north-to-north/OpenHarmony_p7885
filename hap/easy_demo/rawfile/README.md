#  ArkTS语法不支持直接使用globalThis，需要通过一个单例的map来做中转。

- globalThis.ets
```
export class GlobalContext {
  private constructor() {
  }

  private static instance: GlobalContext;
  private _objects = new Map<string, Object>();

  public static getContext(): GlobalContext {
    if (!GlobalContext.instance) {
      GlobalContext.instance = new GlobalContext();
    }
    return GlobalContext.instance;
  }

  getObject(value: string): Object | undefined {
    return this._objects.get(value);
  }

  setObject(key: string, objectClass: Object): void {
    this._objects.set(key, objectClass);
  }
}

export function getGlobalObject(key: string): Object {
  if (GlobalContext.getContext().getObject(key) === undefined) {
    return '';
  }
  return GlobalContext.getContext().getObject(key)!;
}

export function setGlobalObject(key: string, value: Object): void {
  GlobalContext.getContext().setObject(key, value);
}
```

- EntryAbility.ets

```
import { GlobalContext, getGlobalObject, setGlobalObject } from '../pages/GlobalThis'

export default class EntryAbility extends UIAbility {
  onCreate(want: Want, launchParam: AbilityConstant.LaunchParam): void {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');

    //https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/reference/apis/js-apis-inner-application-context.md
    setGlobalObject('this.context', this.context);
  }
```

```
import { GlobalContext, getGlobalObject, setGlobalObject } from '../pages/GlobalThis'

  //用户获取resources/rawfile目录下对应的rawfile文件内容
  private async read_rawfile(file:string){
    try {
      let context = GlobalContext.getContext().getObject('this.context') as Context
      context.resourceManager.getRawFileContent(file, (error: BusinessError, value: Uint8Array) => {
        if (error != null) {
          console.error("error is " + error);
        } else {
          let textDecoder = util.TextDecoder.create("utf-8", { ignoreBOM: true })
          this.message = textDecoder.decodeWithStream(value, { stream: false })
          console.log("getRawFileContent",this.message);
        }
      });
    } catch (error) {
      let code = (error as BusinessError).code;
      let message = (error as BusinessError).message;
      console.error(`callback getRawFileContent failed, error code: ${code}, message: ${message}.`);
    }
  }
```

