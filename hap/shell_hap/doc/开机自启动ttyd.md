## 开机自启动步骤


点击[init.bat脚本](./init.bat)


## 实现思路

- https://laval.csdn.net/649b9578a3b0f00ab37dac66.html

1.`hdc shell`进入shell终端
```
cd /vendor/etc
```

![Alt text](./media/init.uis7885.cfg.png)

发现 init.uis7885.cfg


2.将文件读取到本地

```
hdc file recv /vendor/etc/init.uis7885.cfg D:\gitte_native_repository\OpenHarmony开发者手机
```

3.找到"name"="boot"字符,在"cmds"内容中的最后一行增加字段

```
"exec /bin/sh /data/ttyd_init.sh"
```


2 将init.uis7885.cfg 写入单板

```
hdc shell mount -o remount,rw /vendor
hdc file send init.{产品名称}.cfg  /vendor/etc/
```

3.编写ttyd_init.sh ，内容名称下

```
#!/bin/sh 
echo "ttyd init fail" >/data/ttyd_init.log
cd data
tar -xvf ttyd_oh_install.tar
export LD_LIBRARY_PATH=/data/ttyd_oh_install:$LD_LIBRARY_PATH
echo "ttyd init fail" >/data/ttyd_init.log
nohup ./ttyd_oh_install/ttyd -p 7681 --writable -t disableResizeOverlay=true -w / /bin/sh &
echo "ttyd init success" >/data/ttyd_init.log
```

4.将ttyd_init.sh和ttyd_oh_install.tar发送至开发板端data目录

```
hdc file send ttyd_oh_install.tar /data 
hdc file send ttyd_init.sh /data
```


5.重启设备

```
hdc shell reboot 
```


