
## 实现效果

设置导航栏和状态栏属性实现原理：
在EntryAbility的onWindowStageCreate方法中通过windowStage获取window，然后分别调用setWindowLayoutFullScreen和setWindowSystemBarEnable方法。

| ||
|-|-|
|[window_demo0](./window_demo0/)|[window_demo1](./window_demo1/)|
|![](./snapshot_2024-03-19_12-42-51.jpeg)|![](./snapshot_2024-03-19_12-43-01.jpeg)|

- 获取系统状态栏，导航栏高度
- 设置导航栏，状态栏不可见/不可见
- 设置当前窗口为广色域模式或默认色域模式
- 设置状态栏、导航栏颜色和字体颜色
- 设置窗口的背景色
- 获取系统状态栏，导航栏高度
- 设置禁止截屏/录屏

## 参考文档

- https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/windowmanager/application-window-stage.md#%E4%BD%93%E9%AA%8C%E7%AA%97%E5%8F%A3%E6%B2%89%E6%B5%B8%E5%BC%8F%E8%83%BD%E5%8A%9B
  - https://gitee.com/openharmony/docs/blob/OpenHarmony-4.0-Release/zh-cn/application-dev/windowmanager/application-window-stage.md#%E5%BC%80%E5%8F%91%E6%AD%A5%E9%AA%A4-2

- https://docs.openharmony.cn/pages/v4.1/zh-cn/application-dev/reference/apis/js-apis-window.md#systembarproperties
