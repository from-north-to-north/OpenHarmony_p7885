# Laval-Browser

## 介绍
Browser是[Laval社区](https://laval.csdn.net/)倾心打造的开源浏览器，目前包含网页加载等功能。

## 目录结构
```
├── entry                     # 主业务模块
│   └── src       
│       └── main
│           ├── ets           # 业务代码     
├── Shell                     # 常用脚本
├── signature                 # 签名

```

## 开发环境
**IDE:** DevEco Studio 4.0 Release(Build Version: 4.0.0.600)

**SDK:** Full-Sdk 4.0.10.15

## 指纹证书
```
FBEBB04DAD9EA71201C7BADCBA03FE8A5D64358EB9C06FFD7F5EC2A3E5E25E58
```

## 开发规范
1. 任何组件加载的常量字符串需要定义在`resources`里去引用。
2. 组件尽可能抽离封装。比如ListItem需要单独创建一个Component（就算里面只包含1个Text组件）。
3. 代码解耦。组件放到view文件夹下，模型放在model文件夹下，网络请求及一些业务api的调用放在viewmodel文件夹下。


## 代码提交流程
1. 从本仓fork到自己个人的仓库。
2. 从个人仓库clone到本地。
3. 开发完后提交上传到个人仓库。
4. 个人仓库创建pr到本仓。
5. 仓库管理员权限所有者审核合入。